﻿#region NameSpaces
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.Data.Html;
using Windows.UI.Core;
using testUWP.Helpers;
#endregion

namespace testUWP.Models
{
    /// <summary>
    /// Model to get, refresh and store list news with content
    /// </summary>
    public class PreparedNewsModel : ViewModelBase
    {
        #region Private Members
        private List<SingleNewsData> listNews = new List<SingleNewsData>();
        private Task updationTask;
        #endregion

        #region Constructor
        public PreparedNewsModel()
        {
            updationTask = InitListTitles();
        }
        #endregion

        #region Public Members
        public List<SingleNewsData> ListNews { get { return listNews; } }
        public long NewsCount { get; private set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to start downloading news list task, or retur existed if already started 
        /// </summary>
        public async Task UpdateListNews()
        {
            if (updationTask != null && !updationTask.IsCompleted)
                await updationTask;
            else
            {
                updationTask = InitListTitles();
                await updationTask;
            }
        }

        /// <summary>
        /// Getting single NewsContent task and setting properties to existed object
        /// </summary>
        public async Task<SingleNewsData> GetSingleNewsContent(long Id)
        {
            try
            {
                var response = await ((App)App.Current).Api.GetSingleNews(Id);

                var foundedItem = listNews.Find(n => n.NewsTitle.Id == Id);
                foundedItem.Content = SetMaxHTMLImageWidth(response.Content, 700);
                foundedItem.CreationDate = response.CreationDate;
                foundedItem.ModifiedDate = response.ModifiedDate;
                foundedItem.BankInfoTypeId = response.BankInfoTypeId;
                foundedItem.TypeId = response.TypeId;

                return foundedItem;
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"{exception.Message}");
                return null;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Getting all news list titles
        /// </summary>
        private async Task InitListTitles()
        {
            try
            {
                var response = await ((App)App.Current).Api.GetAllNews();

                if (response != null)
                {
                    NewsCount = response.ListNews.Count;
                    listNews = await AddToListNews(response.ListNews);

                    await RisePropertyChange("ListNews");
                }
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"{exception.Message}");
            }
        }

        /// <summary>
        /// Add news item to stored list if not exist
        /// </summary>
        private async Task<List<SingleNewsData>> AddToListNews(List<NewsTitleModel> initialNewsTitles)
        {
            foreach (var newsTitle in initialNewsTitles)
            {
                if (!listNews.Exists(n => n.NewsTitle.Id == newsTitle.Id))
                {
                    newsTitle.TitleText = HtmlUtilities.ConvertToText(newsTitle.TitleText);

                    listNews.Add( new SingleNewsData(newsTitle) );

                    if (listNews.Count == 20 || listNews.Count % 100 == 0)
                        await RisePropertyChange("ListNews");
                }
            }

            await RisePropertyChange("ListNews");

            return listNews;
        }

        /// <summary>
        /// Rise property change in dispatcher
        /// </summary>
        private async Task RisePropertyChange(string propertyName)
        {
            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            () =>
            {
                OnPropertyChanged(propertyName);
            });
        }

        /// <summary>
        /// Method to set Image max width in html content, and remove some tags if current system is windowsPhone
        /// </summary>
        private string SetMaxHTMLImageWidth(string htmlContent, int maxWidth)
        { 
            var styledImg = $"<img style=\"max-width: {maxWidth}px;\"";
            var resultString = htmlContent.Replace("<img", styledImg);

            if (((App)App.Current).IsWindowsPhone)
            {
                resultString = resultString.Replace("<ul class=\"bull\">", String.Empty);
                resultString = resultString.Replace("<nobr>", String.Empty);
            }

            return resultString;
        }
        #endregion
    }
}
