﻿#region NameSpaces
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
#endregion

namespace testUWP.Models
{
    /// <summary>
    /// model; class for deserialization of the single news title JSON object, the server returns
    /// </summary>
    public class NewsTitleModel
    {
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string EnglishName { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string TitleText { get; set; }
        [JsonProperty(PropertyName = "publicationDate")]
        public DateResponse PublicationDate { get; set; }
        [JsonProperty(PropertyName = "bankInfoTypeId")]
        public int BankInfoTypeId { get; set; }
    }

    /// <summary>
    /// model; class for deserialization of the date JSON object, the server returns in miliseconds
    /// </summary>
    public class DateResponse
    {
        [JsonProperty(PropertyName = "milliseconds")]
        public long Milliseconds { get; set; }
    }

    /// <summary>
    /// model; class for deserialization of the list News titles JSON object, the server returns
    /// </summary>
    public class NewsResponse
    {
        [JsonProperty(PropertyName = "resultCode")]
        public string ResultCode { get; set; }
        [JsonProperty(PropertyName = "payload")]
        public List<NewsTitleModel> ListNews { get; set; }
    }

    /// <summary>
    /// model; class for deserialization of the singleNews JSON object, the server returns
    /// </summary>
    public class SingleNewsResponse
    {
        [JsonProperty(PropertyName = "resultCode")]
        public string ResultCode { get; set; }
        [JsonProperty(PropertyName = "payload")]
        public SingleNewsData Data { get; set; }
    }

    /// <summary>
    /// model; class to deserialise single News json object
    /// </summary>
    public class SingleNewsData
    {
        [JsonProperty(PropertyName = "title")]
        public NewsTitleModel NewsTitle { get; set; }
        [JsonProperty(PropertyName = "creationDate")]
        public DateResponse CreationDate { get; set; }
        [JsonProperty(PropertyName = "lastModificationDate")]
        public DateResponse ModifiedDate { get; set; }
        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }
        [JsonProperty(PropertyName = "bankInfoTypeId")]
        public int BankInfoTypeId { get; set; }
        [JsonProperty(PropertyName = "typeId")]
        public string TypeId { get; set; }

        public SingleNewsData()
        { }

        public SingleNewsData(NewsTitleModel title)
        {
            NewsTitle = title;
            Content = String.Empty;
        }
    }
}
