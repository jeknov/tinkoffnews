﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using testUWP.Models;
#endregion

namespace testUWP.Helpers
{
    public class TinkoffApi
    {
        #region private Constants
        private const string baseUrl = "https://api.tinkoff.ru/v1/";
        private const string allNews = "news"; //endpoint for getting all news titles
        private const string singleNews = "news_content?id="; //endpoint for getting single news data by id
        #endregion

        #region Private Members

        private HttpClient client;

        #endregion

        #region Constructor

        public TinkoffApi()
        {
            client = PrepareHttpClient();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get request, reurn list news titles
        /// </summary>
        public async Task<NewsResponse> GetAllNews()
        {
            try
            {
                var response = await client.GetAsync(allNews);

                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                if (String.IsNullOrEmpty(content))
                    return null;

                var newsResponse = JsonConvert.DeserializeObject<NewsResponse>(content);
                return newsResponse;
            }
            catch (Exception exception)
            {
                //handle exception someway
                var preparedMessage = PrepareExceptionMessage(exception);
                System.Diagnostics.Debug.WriteLine( $"Exception getting news: {preparedMessage}" );

                return null;
            }
        }

        /// <summary>
        /// Get request, reurn message content by id
        /// </summary>
        public async Task<SingleNewsData> GetSingleNews(long id)
        {
            try
            {
                var response = await client.GetAsync( $"{singleNews}{id}" );

                response.EnsureSuccessStatusCode();

                var content = await response.Content.ReadAsStringAsync();

                if (String.IsNullOrEmpty(content))
                    return null;

                var newsResponse = JsonConvert.DeserializeObject<SingleNewsResponse>(content);

                if (newsResponse == null && newsResponse.Data != null)
                    throw new Exception($"response or response.data is null");

                return newsResponse.Data;
            }
            catch (Exception exception)
            {
                //handle exception some way
                var preparedMessage = PrepareExceptionMessage(exception);
                System.Diagnostics.Debug.WriteLine($"Exception getting single news: {preparedMessage}");

                return null;
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Method to initialise httpclien object
        /// </summary>
        private HttpClient PrepareHttpClient()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
        
        /// <summary>
        /// Method to build exception message string
        /// </summary>
        private static string PrepareExceptionMessage(Exception exception)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append( $"{ exception.Message}. StackTrace: { exception.StackTrace}" );
            stringBuilder.AppendLine( $" Source: {exception.Source}" );
            stringBuilder.AppendLine( $"[{DateTime.Now.ToLocalTime()}]" );

            return stringBuilder.ToString();
        }

        #endregion
    }
}
