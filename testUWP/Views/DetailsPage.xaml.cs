﻿#region NameSpaces
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Animation;
#endregion


namespace testUWP.Views
{
    /// <summary>
    /// Selected message details page, display message content
    /// </summary>
    public sealed partial class DetailsPage : Page
    {
        #region Constructor

        public DetailsPage()
        {
            this.InitializeComponent();
            this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;
        }

        #endregion

        #region Navigation Handlers
        /// <summary>
        /// setting data context from parameter; Initialising back button
        /// </summary>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var viewModel = e.Parameter as NewsListViewModel;
            this.DataContext = viewModel;
            
            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested += OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
        }

        /// <summary>
        /// collapse back button
        /// </summary>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SystemNavigationManager systemNavigationManager = SystemNavigationManager.GetForCurrentView();
            systemNavigationManager.BackRequested -= OnBackRequested;
            systemNavigationManager.AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        #endregion

        #region Private handlers
        /// <summary>
        /// Subscribe on window size changed
        /// </summary>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            Window.Current.SizeChanged += OnWindowSizeChanged;
        }

        /// <summary>
        /// handle back button click
        /// </summary>
        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            // Mark event as handled so we don't get bounced out of the app.
            e.Handled = true;

            Frame.Navigate(typeof(ListNewsPage), "Back", new EntranceNavigationTransitionInfo());
        }

        /// <summary>
        /// Unsubscribe from window resize event
        /// </summary>
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            Window.Current.SizeChanged -= OnWindowSizeChanged;
        }

        /// <summary>
        /// Handle window resize event, switch to wide page mode (going back to listNewsPage)
        /// </summary>
        private void OnWindowSizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            if (ShouldGoToWideState())
            {
                // Make sure we are no longer listening to window change events.
                Window.Current.SizeChanged -= OnWindowSizeChanged;

                // We shouldn't see this page since we are in "wide" mode.
                NavigateBackForWideState(useTransition: false);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Switch to wide page mode (going back to listNewsPage)
        /// </summary>
        private void NavigateBackForWideState(bool useTransition)
        {
            // Evict this page from the cache as we may not need it again.
            NavigationCacheMode = NavigationCacheMode.Disabled;

            if (useTransition)
            {
                Frame.GoBack(new EntranceNavigationTransitionInfo());
            }
            else
            {
                Frame.GoBack(new SuppressNavigationTransitionInfo());
            }
        }

        /// <summary>
        /// Check if window width more than 720
        /// </summary>
        private bool ShouldGoToWideState()
        {
            return Window.Current.Bounds.Width >= 720;
        }

        #endregion
    }
}
