﻿#region Namespaces
using System;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
#endregion

namespace testUWP.Views
{
    /// <summary>
    ///Main page displaying list news titles adn its content
    /// </summary>
    public sealed partial class ListNewsPage : Page
    {
        #region Private Members

        private NewsListViewModel newsListViewModel;
        
        #endregion

        #region Constructor

        public ListNewsPage()
        {
            this.InitializeComponent();

            if (newsListViewModel == null)
                newsListViewModel = new NewsListViewModel();

            this.DataContext = newsListViewModel;
            this.Loaded += OnLoaded;
        }

        #endregion

        #region Private handlers
        /// <summary>
        /// select page state (wide/narrow)
        /// </summary>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (PageSizeStatesGroup.CurrentState == NarrowState)
            {
                VisualStateManager.GoToState(this, NarrowState.Name, true);
            }
            else if (PageSizeStatesGroup.CurrentState == WideState)
            {
                VisualStateManager.GoToState(this, WideState.Name, true);
            }
            else
            {
                new InvalidOperationException();
            }
        }

        /// <summary>
        /// Handle page state (wide/narrow) changing
        /// </summary>
        private void OnCurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {
            bool isNarrow = e.NewState == NarrowState;
            
            if(!isNarrow)
            {
                VisualStateManager.GoToState(this, WideState.Name, true);
            }

            EntranceNavigationTransitionInfo.SetIsTargetElement(ListViewTitles, isNarrow);

            if (DetailsGrid != null)
            {
                EntranceNavigationTransitionInfo.SetIsTargetElement(DetailsGrid, !isNarrow);
            }
        }

        /// <summary>
        /// Rise method from viewmodel if page in narrow state (go to details page)
        /// </summary>
        private void OnItemClick(object sender, ItemClickEventArgs e)
        {
            if (PageSizeStatesGroup.CurrentState == NarrowState)
            {
                newsListViewModel.NavigateToDetails();
            }
        }

        /// <summary>
        /// Rise method from viewmodel if refresh list view requested
        /// </summary>
        private async void ListViewRefreshRequested(object sender, RefreshableListView.RefreshRequestedEventArgs e)
        {
            using (Deferral deferral = ListViewTitles.AutoRefresh ? e.GetDeferral() : null)
            {
                await newsListViewModel.ChekForUpdatesListNews();//await FetchAndInsertItemsAsync();

                if (SpinnerStoryboard.GetCurrentState() != ClockState.Stopped)
                {
                    SpinnerStoryboard.Stop();
                }
            }
        }

        /// <summary>
        /// Method to start/stop refreshing animation
        /// </summary>
        private void ListViewPullProgressChanged(object sender, RefreshableListView.RefreshProgressEventArgs e)
        {
            if (e.IsRefreshable)
            {
                if (e.PullProgress == 1)
                {
                    // Progress = 1.0 means that the refresh has been triggered.
                    if (SpinnerStoryboard.GetCurrentState() == ClockState.Stopped)
                    {
                        SpinnerStoryboard.Begin();
                    }
                }
                else if (SpinnerStoryboard.GetCurrentState() != ClockState.Stopped)
                {
                    SpinnerStoryboard.Stop();
                }
                else
                {
                    // Turn the indicator by an amount proportional to the pull progress.
                    SpinnerTransform.Angle = e.PullProgress * 360;
                }
            }
        }

        #endregion
    }
}
