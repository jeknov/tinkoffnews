﻿#region Namespaces
using System;
using System.Threading.Tasks;
using System.Linq;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using testUWP.Models;
using testUWP.Helpers;
#endregion

namespace testUWP
{
    public class NewsListViewModel : ViewModelBase
    {
        #region Private members

        private bool isUpdating;
        private SingleNewsData selectedNews;
        private PreparedNewsModel preparedNewsModel;
        private ObservableCollection<SingleNewsData> listNews = new ObservableCollection<SingleNewsData>();

        #endregion
        
        /// <summary>
        /// Constructor of NewsListViewModel
        /// </summary>
        #region Constructor

        public NewsListViewModel()
        {
            preparedNewsModel = ((App)App.Current).PreparedNewsModel;
            preparedNewsModel.PropertyChanged += HandlePropertyChanged;

            IsUpdating = true;
        }

        #endregion

        /// <summary>
        /// Bindable properties 
        /// </summary>
        #region Public Members

        /// <summary>
        /// Property to indicate that news list is updating now
        /// </summary>
        public bool IsUpdating
        {
            get { return isUpdating; }
            set
            {
                if (isUpdating == value)
                    return;

                isUpdating = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Current list news
        /// </summary>
        public ObservableCollection<SingleNewsData> ListNews
        {
            get
            {
                return listNews;
            }
            set
            {
                listNews = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Selected message from listNews
        /// </summary>
        public SingleNewsData SelectedNews
        {
            get
            {
                return selectedNews;
            }
            set
            {
                selectedNews = value;

                if (String.IsNullOrEmpty(selectedNews.Content))
                    GetNewsContent(selectedNews.NewsTitle.Id);

                OnPropertyChanged();
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Method to navigete to details message page 
        /// </summary>
        public void NavigateToDetails()
        {
            var frame = Window.Current.Content as Frame;

            if (frame == null)
                return;

            if (frame.Content is Page && frame.Content is Views.ListNewsPage)
                frame.Navigate(typeof(Views.DetailsPage), this, new DrillInNavigationTransitionInfo());
        }

        /// <summary>
        /// Method to start updating list news task in storeModel
        /// </summary>
        public async Task ChekForUpdatesListNews()
        {
            // Show the status bar progress indicator, if available.
            Windows.UI.ViewManagement.StatusBar statusBar = null;
            if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
                statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();

            if (statusBar != null)
            {
                var task = statusBar.ProgressIndicator.ShowAsync();
            }

            // Simulate delay while we go fetch new items.
            await Task.Delay(500);
            await preparedNewsModel.UpdateListNews();

            if (statusBar != null)
            {
                var task = statusBar.ProgressIndicator.HideAsync();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Handle list news updated in storeModel, update currentlistNews
        /// </summary>
        private void HandlePropertyChanged(object sender, PropertyChangedEventArgs arguments)
        {
            if (arguments.PropertyName == "ListNews")
            {
                foreach (var newsItem in preparedNewsModel.ListNews)
                {
                    if (!ListNews.Contains(newsItem))
                    {
                        if (listNews.Count == 0 || listNews[0] == null || newsItem.NewsTitle.PublicationDate == null)
                            ListNews.Add(newsItem);
                        else if (newsItem.NewsTitle.PublicationDate.Milliseconds < listNews[0].NewsTitle.PublicationDate.Milliseconds)
                            ListNews.Add(newsItem);
                        else
                            ListNews.Insert(0, newsItem);
                    }
                }

                if (ListNews.Count == preparedNewsModel.NewsCount)
                    IsUpdating = false;
            }
        }

        /// <summary>
        /// Method to get Message Content by ID from storeModel, store downloads content if needed
        /// </summary>
        private async Task GetNewsContent(long Id)
        {
            try
            {
                var cachedNewsItem = preparedNewsModel.ListNews.Find(n => n.NewsTitle.Id == Id);
                if (string.IsNullOrEmpty(cachedNewsItem.Content))
                    ListNews.First(n => n.NewsTitle.Id == Id).Content = (await preparedNewsModel.GetSingleNewsContent(Id)).Content;

                OnPropertyChanged("SelectedNews");
            }
            catch (Exception exception)
            {
                System.Diagnostics.Debug.WriteLine($"Can't get message content: {exception.Message}");
            }
        }

        #endregion
    }
}
