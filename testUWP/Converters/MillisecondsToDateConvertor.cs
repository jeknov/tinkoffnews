﻿using System;
using Windows.UI.Xaml.Data;

namespace testUWP.Converters
{
    public class MillisecondsToDateConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            try
            {
                dateTime = dateTime.AddMilliseconds(System.Convert.ToDouble(value));

                var currentDateTime = DateTime.Now;

                if (dateTime.Date == DateTime.Today.Date)
                    return $"Сегодня {dateTime.ToString("HH:mm:ss")}";
                else if (dateTime.Date == DateTime.Today.AddDays(-1).Date)
                    return $"Вчера {dateTime.ToString("HH:mm:ss")}";
                else
                    return dateTime.ToString("dd.MM.yyyy  HH:mm:ss");
            }
            catch (Exception exception)
            {
                return dateTime.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
